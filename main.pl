#!/usr/bin/env perl

use File::Path 'make_path';
use Getopt::ArgParse;
use LWP::Simple;
use Web::Query;
use URI::URL;
use warnings;
use strict;

sub getargs {
    my $ap = Getopt::ArgParse->new_parser(
	prog => "reddit_gallery_dl",
	description => "Reddit Gallery Downloader",
	);

    $ap->add_arg("--url", "-u", required => 1);
    $ap->add_arg("--path", "-p", required => 1);

    my $ns = $ap->parse_args(@ARGV);
}

sub trim_link{
    my ($url) = @_;
    my $u = URI::URL->new($url);
    my $host = $u->netloc;
    my $path = $u->path;

    sprintf "https://%s%s", $host, $path;
}

sub get_links {
    my ($page, $query) = @_;
    my $links = $page->find($query)
	->map(
	sub {
	    my ($i, $elem) = @_;
	    return $elem->attr("href");
	});
}

sub get_images {
    my ($url, $path) = @_;

    # Die if you cannot get the images
    my $q = wq($url) || die sprintf("Cannot get URL: %s", $url);

    make_path($path); # Aborts by default on failure.

    my $selector = "div.media-preview-content a.may-blank";
    my $links = get_links $q, $selector;

    for (my $i = 0; $i < @$links; $i++) {
	my $source = $links->[$i];
	my $trimurl = trim_link $source;
	my $ext = (split /\./, $trimurl)[-1];
	my $endpath = sprintf('%s/%03d.%s', $path, $i, $ext);

	print ("Downloading... path: $endpath, link: $source\n");

	my $ret = getstore($source, $endpath);

	unless (is_success($ret)) { # Die at first failure to save time.
	    die sprintf("Could not store url: %s, in path: %s\n", $source, $endpath);
	}
    }

    print "Content downloaded.\n";
}

sub main {
    my $ns = getargs();
    my $path = $ns->path;
    my $url = $ns->url;

    get_images $url, $path;
}

main()
