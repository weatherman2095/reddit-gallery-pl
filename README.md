# Note about the current state of the program

This stopped working since some major changes happened to Reddit in
the meantime, this effectively requires a rewrite.

# Introduction

This is a relatively trivial script to download images from a Reddit
gallery page.

It's faster than right-clicking & saving manually.

The point is largely for this to serve as an exercise to get a bit
more familiar with Perl.
